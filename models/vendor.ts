import mongoose, {Schema, Document, Model} from "mongoose";

interface vendorDoc extends Document{
    name:string;            // DTO- Vendor or Company Name
    ownerName:string;       // DTO- Name of the Owner
    phone:string;           // DTO- 
    address:string;         // DTO-
    pincode:string;         // DTO- AC verification received by SMS/Call
    email:string;           // DTO- 
    password:string;        // DTO-
    salt:string;
    serviceAvailable:boolean; // Hashed to serve as login token.
    services:[string];        // list of verndor services.
    rating:number;            // Rating of the company or vendor
    coverImages:[string];     // List of images
    products: any;            // all products  
}

const vendorSchema = new Schema({
        name:{ type: String, required: true },      // DTO- Required
        ownerName:{ type: String, required: true }, // DTO- Required
        phone:{ type: String, required: true },     // DTO- Required
        address:{ type: String, required:true },    // DTO- Required
        pincode:{ type: String, required: true },   // DTO- Required
        email:{ type: String, required: true },     // DTO- Required
        password:{ type: String, required: true },  // DTO- Required
        salt:{ type: String, required: true },      // Required
        services:{ type: [String] },                //  
        serviceAvailable: {type: Boolean },         //  
        rating:{ type: Number, required: true },    // Required
        coverImages:{type: [String]},               //  
        products: [{                                //  
            type: mongoose.SchemaTypes.ObjectId,    //
            ref:'products'                          //
        }]                                
    },  {
            toJSON:{
                transform(doc,ret){
                    delete ret.password;
                    delete ret.salt;
                    delete ret.__v;
                    delete ret.createdAt;
                    delete ret.updatedAt;
                }
            },
            
            timestamps:true // creates timestamp for each record
    });


// Creating model, vendorDoc validates the type of data it should recieve
const Vendor = mongoose.model<vendorDoc>('Vendor', vendorSchema);

export {Vendor};