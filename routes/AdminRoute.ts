import express, {Request, Response, NextFunction} from 'express';
import { createVendor, getVendorByID, getVendors } from '../controllers';

const router = express.Router();

router.post('/vendor',createVendor);
router.get('/vendors',getVendors);
router.get('/vendor/:id',getVendorByID);

router.get('/',(req:Request,res:Response,next:NextFunction)=>{
    return res.json({message:"Admin Route is working"});
});


export {router as AdminRoute};