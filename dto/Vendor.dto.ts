// Data Transfer Object (DTO) for Vendors
// Input data comes from UI. Some extra info will be augmented later by Model
export interface createVendorInputs{
    name:string;        // DTO- Vendor or Company Name
    ownerName:string;   // DTO- Name of the Owner
    phone:string;       // DTO-
    address:string;     // DTO-
    pincode:string;     // DTO- AC verification received by SMS/Call
    email:string;       // DTO- 
    password:string;    // DTO-
}