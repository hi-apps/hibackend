import { Request, Response, NextFunction } from "express";
import { createVendorInputs } from "../dto";
import { Vendor } from "../models";
import { GeneratePassword, GenerateSalt } from "../utility";

export const createVendor  = async (req:Request, res:Response, next:NextFunction)=>{
    const {name, ownerName, phone, address, pincode, email, password} = <createVendorInputs>req.body;

    const existsVendor = await Vendor.findOne({email:email});

    if(existsVendor != null){
        return res.status(201).json({message:"This "+email+" vendor already exists."});
    }

    // Generate the salt
    const salt = await GenerateSalt();
    // Encrypt Password using the salt
    const saltyPassword = await GeneratePassword(password,salt);


    const saveVendor = await Vendor.create({ // use await bcz it returns kinda promise
        name:name,           // DTO-  Required
        ownerName:ownerName, // DTO-  Required
        phone:phone,         // DTO-  Required
        address:address,     // DTO-  Required
        pincode:'23423',     // DTO-  Required
        email:email,         // DTO-  Required
        password:saltyPassword,   // DTO-  Required
        salt:salt,                 //  Required
        services:[],                //
        rating:0,                   //  Required
        coverImages:[],             //
        serviceAvailable:false,     //
        products:[],                //
    })
    
    return res.json(saveVendor); 
}
export const getVendors  = async (req:Request, res:Response, next:NextFunction)=>{
    const vendors = await Vendor.find({});
    
    if(vendors!=null){
        return res.json(vendors);
    }
    
    return res.json({message:"No vendor found"})
}
export const getVendorByID  = async (req:Request, res:Response, next:NextFunction)=>{

}
