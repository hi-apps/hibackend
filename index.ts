import express from 'express';
import { Request, Response, NextFunction } from 'express';
import { AdminRoute, VendorRoute } from './routes';
import mongoose from 'mongoose';
import { MONGO_URI } from './config';

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use((req:Request, res:Response, next: NextFunction) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type");
    next();
  })

app.use('/admin',AdminRoute);
app.use('/vandor',VendorRoute);

mongoose.connect(MONGO_URI,{}).then(result=>{
  console.log('DB Connected');
}).catch(error=>{console.log('Error:\t'+error)});

const port = process.env.PORT || 8000;
app.listen(port,()=>{
    //console.clear();
    console.log("Server is running on port: "+port);
});